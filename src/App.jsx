import React from "react";
import Display from "./Display";
import Input from "./Input";
import CPUInfo from "./CPUInfo";
import Docs from "./Docs";
export default class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: {}
    };
  }

  render() {
    return (
      <div className="main">
        <Display data={this.state.data}></Display>
        <Input
          updateState={d => {
            this.setState({ data: d });
          }}
        ></Input>
        <CPUInfo data={this.state.data}></CPUInfo>
        <Docs></Docs>
      </div>
    );
  }
}
