import React, { Component } from "react";
let notImplemented = [
  {
    pattern: "EX9E",
    category: "KeyOp",
    equivalent: "if(key()==Vx)",
    desc:
      "Skips the next instruction if the key stored in VX is pressed. (Usually the next instruction is a jump to skip a code block)"
  },
  {
    pattern: "EXA1",
    category: "KeyOp",
    equivalent: "if(key()!=Vx)",
    desc:
      "Skips the next instruction if the key stored in VX isn't pressed. (Usually the next instruction is a jump to skip a code block)"
  },
  {
    pattern: "FX07",
    category: "Timer",
    equivalent: "Vx = get_delay()",
    desc: "Sets VX to the value of the delay timer."
  },
  {
    pattern: "FX0A",
    category: "KeyOp",
    equivalent: "Vx = get_key()",
    desc:
      "A key press is awaited, and then stored in VX. (Blocking Operation. All instruction halted until next key event)"
  },
  {
    pattern: "FX15",
    category: "Timer",
    equivalent: "delay_timer(Vx)",
    desc: "Sets the delay timer to VX."
  },
  {
    pattern: "FX18",
    category: "Sound",
    equivalent: "sound_timer(Vx)",
    desc: "Sets the sound timer to VX."
  },
  {
    pattern: "0NNN",
    category: "Call",
    equivalent: "",
    desc: "	Calls RCA 1802 program at address NNN. Not necessary for most ROMs."
  }
];
let commands = [
  {
    pattern: "00E0",
    category: "Display",
    equivalent: "disp_clear()",
    desc: "Clears the screen."
  },
  {
    pattern: "00EE",
    category: "Flow",
    equivalent: "return;",
    desc: "Returns from a subroutine."
  },
  {
    pattern: "1 NNN",
    category: "Flow",
    equivalent: "goto NNN;",
    desc: "Jumps to address NNN."
  },
  {
    pattern: "2 NNN",
    category: "Flow",
    equivalent: "*(0xNNN)()",
    desc: "Calls subroutine at NNN."
  },
  {
    pattern: "3 X NN",
    category: "Cond",
    equivalent: "if(Vx==NN)",
    desc:
      "Skips the next instruction if VX equals NN. (Usually the next instruction is a jump to skip a code block)"
  },
  {
    pattern: "4 X NN",
    category: "Cond",
    equivalent: "if(Vx!=NN)",
    desc:
      "Skips the next instruction if VX doesn't equal NN. (Usually the next instruction is a jump to skip a code block)"
  },
  {
    pattern: "5 X Y 0",
    category: "Cond",
    equivalent: "if(Vx==Vy)",
    desc:
      "Skips the next instruction if VX equals VY. (Usually the next instruction is a jump to skip a code block)"
  },
  {
    pattern: "6 X NN",
    category: "Const",
    equivalent: "Vx = NN",
    desc: "Sets VX to NN."
  },
  {
    pattern: "7 X NN",
    category: "Const",
    equivalent: "Vx += NN",
    desc: "Adds NN to VX. (Carry flag (VF) is not changed)"
  },
  {
    pattern: "8 X Y 0",
    category: "Assign",
    equivalent: "Vx=Vy",
    desc: "Sets VX to the value of VY."
  },
  {
    pattern: "8 X Y 1",
    category: "BitOp",
    equivalent: "Vx=Vx|Vy",
    desc: "Sets VX to VX or VY. (Bitwise OR operation)"
  },
  {
    pattern: "8 X Y 2",
    category: "BitOp",
    equivalent: "Vx=Vx&Vy",
    desc: "Sets VX to VX and VY. (Bitwise AND operation)"
  },
  {
    pattern: "8 X Y 3",
    category: "BitOp",
    equivalent: "Vx=Vx^Vy",
    desc: "Sets VX to VX xor VY."
  },
  {
    pattern: "8 X Y 4",
    category: "Math",
    equivalent: "Vx += Vy",
    desc:
      "Adds VY to VX. VF is set to 1 when there's a carry, and to 0 when there isn't."
  },
  {
    pattern: "8 X Y 5",
    category: "Math",
    equivalent: "Vx -= Vy",
    desc:
      "VY is subtracted from VX. VF is set to 0 when there's a borrow, and 1 when there isn't."
  },
  {
    pattern: "8 X Y 6",
    category: "BitOp",
    equivalent: "Vx>>=1",
    desc:
      "Stores the least significant bit of VX in VF and then shifts VX to the right by 1.[2]"
  },
  {
    pattern: "8 X Y 7",
    category: "Math",
    equivalent: "Vx=Vy-Vx",
    desc:
      "Sets VX to VY minus VX. VF is set to 0 when there's a borrow, and 1 when there isn't."
  },
  {
    pattern: "8 X Y E",
    category: "BitOp",
    equivalent: "Vx<<=1",
    desc:
      "Stores the most significant bit of VX in VF and then shifts VX to the left by 1.[3]"
  },
  {
    pattern: "9 X Y 0",
    category: "Cond",
    equivalent: "if(Vx!=Vy)",
    desc:
      "Skips the next instruction if VX doesn't equal VY. (Usually the next instruction is a jump to skip a code block)"
  },
  {
    pattern: "A NNN",
    category: "MEM",
    equivalent: "I = NNN",
    desc: "Sets I to the address NNN."
  },
  {
    pattern: "B NNN",
    category: "Flow",
    equivalent: "PC=V0+NNN",
    desc: "Jumps to the address NNN plus V0."
  },
  {
    pattern: "C X NN",
    category: "Rand",
    equivalent: "Vx=rand()&NN",
    desc:
      "Sets VX to the result of a bitwise and operation on a random number (Typically: 0 to 255) and NN."
  },
  {
    pattern: "D X Y N",
    category: "Display",
    equivalent: "draw(Vx,Vy,N)",
    desc:
      "Draws a sprite at coordinate (VX, VY) that has a width of 8 pixels and a height of N pixels. Each row of 8 pixels is read as bit-coded starting from memory location I; I value doesn’t change after the execution of this instruction. As described above, VF is set to 1 if any screen pixels are flipped from set to unset when the sprite is drawn, and to 0 if that doesn’t happen"
  },

  {
    pattern: "F X 1E",
    category: "MEM",
    equivalent: "I +=Vx",
    desc: "Adds VX to I.[4]"
  },
  {
    pattern: "F X 29",
    category: "MEM",
    equivalent: "I=sprite_addr[Vx]",
    desc:
      "Sets I to the location of the sprite for the character in VX. Characters 0-F (in hexadecimal) are represented by a 4x5 font."
  },
  {
    pattern: "F X 33",
    category: "BCD",
    equivalent: ``,
    desc:
      "Stores the binary-coded decimal representation of VX, with the most significant of three digits at the address in I, the middle digit at I plus 1, and the least significant digit at I plus 2. (In other words, take the decimal representation of VX, place the hundreds digit in memory at location in I, the tens digit at location I+1, and the ones digit at location I+2.)"
  },
  {
    pattern: "F X 55",
    category: "MEM",
    equivalent: "reg_dump(Vx,&I)",
    desc:
      "Stores V0 to VX (including VX) in memory starting at address I. The offset from I is increased by 1 for each value written, but I itself is left unmodified."
  },
  {
    pattern: "F X 65",
    category: "MEM",
    equivalent: "reg_load(Vx,&I)",
    desc:
      "Fills V0 to VX (including VX) with values from memory starting at address I. The offset from I is increased by 1 for each value written, but I itself is left unmodified."
  }
];

export default class Docs extends Component {
  render() {
    return (
      <div>
        <h2>Docs:</h2>
        The machine has a total of 4096 bytes of memory.
        <br />
        Your program begins at memory location 0x200
        <br />
        <br />
        The first and the last 512 bytes of memory are protected and{" "}
        <b>TOP SECRET.</b>
        <br />
        You can use <b>;</b> or <b>'</b> for comments.
        <h3>Commands:</h3>
        <div>
          {commands
            .sort((a, b) => a.category.localeCompare(b.category))
            .map(c => {
              return <Comd {...c}></Comd>;
            })}
        </div>
        <div style={{ height: 24 }}></div>
        <h3>Not implemented:</h3>
        <div>
          {notImplemented
            .sort((a, b) => a.category.localeCompare(b.category))
            .map(c => {
              return <Comd {...c}></Comd>;
            })}
        </div>
      </div>
    );
  }
}
class Comd extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      marked: false
    };
  }

  render() {
    let { pattern, category, equivalent, desc } = this.props;
    return (
      <div
        className={"doc-row " + (this.state.marked ? "marked " : " ")}
        onClick={() => this.setState({ marked: !this.state.marked })}
      >
        <div className="doc-pattern">{pattern}</div>
        <div className="doc-cat">{category}</div>
        <div className="doc-equivalent">{equivalent}</div>
        <div className="doc-desc">{desc}</div>
      </div>
    );
  }
}
