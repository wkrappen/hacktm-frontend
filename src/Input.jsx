import React, { Component } from "react";
import axios from "axios";
export default class Input extends Component {
  componentDidMount() {
    this.fetch();
  }
  fetch(step) {
    let postData = { code: this.state.input };
    if (step) {
      postData.step = this.state.step;
    }

    axios.post("http://localhost:3000/", postData).then(d => {
      console.log(d);
      if (d.data.data) {
        this.props.updateState({
          ...d.data.data,
          message: d.data.message || ""
        });
      }
    });
  }
  constructor(props) {
    super(props);

    this.state = {
      input: "",
      step: 0
    };
  }
  run = () => {
    this.setState({ step: 0 }, () => {
      this.fetch();
    });
  };
  step = () => {
    this.setState({ step: this.state.step + 1 }, () => {
      this.fetch(true);
    });
  };
  step10 = () => {
    this.setState({ step: this.state.step + 10 }, () => {
      this.fetch(true);
    });
  };
  reset = () => {
    this.setState({ step: 0 }, () => {
      this.props.updateState({ pc: 0x200, i: 0x200 });
    });
  };
  render() {
    return (
      <div className="inputWrapper">
        <textarea
          className="mainInput"
          value={this.state.input}
          onChange={e => {
            this.setState({ input: e.target.value });
          }}
        ></textarea>
        <button onClick={this.run}>Run</button>
        <button onClick={this.step}>Step</button>
        <button onClick={this.step10}>Step 10</button>
        <button onClick={this.reset}>Reset</button>
      </div>
    );
  }
}
