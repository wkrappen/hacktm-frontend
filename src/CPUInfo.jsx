import React, { Component } from "react";

export default class CPUInfo extends Component {
  render() {
    let d = this.props.data || {};
    return (
      <div className="cpuInfo">
        <KeyValue title="Error message">{d.message}</KeyValue>
        <KeyValue title="PC">
          {d.pc} (0x{i2h(d.pc, 4)})
        </KeyValue>
        <KeyValue title="I">
          {d.i} (0x{i2h(d.i, 4)})
        </KeyValue>
        <KeyValue title="Cycles">{d.cycles}</KeyValue>
        <KeyValue title="Last instruction">{d.lastInstruction}</KeyValue>
        <KeyValue title="Stack">
          {d.stack &&
            d.stack
              .slice()
              .reverse()
              .map(v => <div>{v}</div>)}
        </KeyValue>
        <KeyValue title="Registers">
          {d.v &&
            d.v.map((v, i) => {
              return (
                <div style={{ textAlign: "right" }}>
                  <b>V{i}: </b>
                  {v} (0x{i2h(v)})
                </div>
              );
            })}
        </KeyValue>
      </div>
    );
  }
}

function KeyValue({ title, children }) {
  return (
    <div className="keyValue">
      <div className="kvTitle">{title}</div>
      <div className="kvValue">{children}</div>
    </div>
  );
}

function h2i(h = "0") {
  return parseInt(h, 16);
}

function i2h(i = 0, pad = 2) {
  return i
    .toString(16)
    .padStart(pad, "0")
    .toUpperCase();
}
