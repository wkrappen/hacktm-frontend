import React, { Component } from "react";

export default class Display extends Component {
  render() {
    let rows =
      this.props.data.display ||
      new Array(32).fill(0).map(() => Array(64).fill(0));
    return (
      <div className="display">
        {rows.map(r => {
          return (
            <div className={"display-row"}>
              {r.map(c => {
                return (
                  <div className={"cell " + (c == 1 ? "on" : "off")}></div>
                );
              })}
            </div>
          );
        })}
      </div>
    );
  }
}
